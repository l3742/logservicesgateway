package ar.com.redlink.logservicesgateway.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component("HealthCheck")
public class ActuatorBeanConfig implements HealthIndicator{

	@Override
	public Health health() {
		return Health.up().build();
	}

}
