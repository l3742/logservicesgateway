package ar.com.redlink.logservicesgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@SpringBootApplication
public class LogServicesGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogServicesGatewayApplication.class, args);
	}

}
