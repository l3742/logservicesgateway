apiVersion: v1
kind: Template
metadata:
  annotations:
    openshift.io/display-name: ${APP_NAME}
  name: ${APP_NAME}
objects:
  -
    apiVersion: v1
    kind: Service
    metadata:
      labels:
        app: ${APP_NAME}
      name: ${APP_NAME}
    spec:
      ports:
        -
          name: 8080-tcp
          port: 8080
          protocol: TCP
          targetPort: 8080
        -
          name: 8443-tcp
          port: 8443
          protocol: TCP
          targetPort: 8443
      sessionAffinity: None
      type: ClusterIP
      selector:
        name: ${APP_NAME}
  - apiVersion: route.openshift.io/v1
    kind: Route
    metadata:
      labels:
        app: ${APP_NAME}
      name: ${APP_NAME}
    spec:
      port:
        targetPort: 8080-tcp
      to:
        kind: Service
        name: ${APP_NAME}
        weight: 100
      wildcardPolicy: None
  -
    apiVersion: apps.openshift.io/v1
    kind: DeploymentConfig
    metadata:
      name: ${APP_NAME}
      labels:
        name: ${APP_NAME}
        app.kubernetes.io/name: spring-boot
        app.kubernetes.io/part-of: logservices
      annotations:
        openshift.io/display-name: ${APP_NAME}
    spec:
      progressDeadlineSeconds: 600
      replicas: 1
      revisionHistoryLimit: 10
      selector:
        app: ${APP_NAME}
        deploymentconfig: ${APP_NAME}
        name: ${APP_NAME}
      strategy:
        type: Rolling
      template:
        metadata:
          labels:
            app: ${APP_NAME}
            deploymentconfig: ${APP_NAME}
            name: ${APP_NAME}
          annotations:
            sidecar.istio.io/inject: 'true'
        spec:
          volumes:
            - name: certs
              secret:
                secretName: link-secret-truststore
                items:
                  - key: truststore.jks
                    path: truststore.jks
          containers:
            -
              name: ${APP_NAME}
              image: "image-registry.openshift-image-registry.svc:5000/link-app-ivr-desa/${APP_NAME}:latest"
              imagePullPolicy: Always
              ports:
                -
                  containerPort: 8080
                  protocol: TCP
                -
                  containerPort: 8443
                  protocol: TCP
              volumeMounts:
                - name: certs
                  readOnly: true
                  mountPath: /certs/truststore.jks
                  subPath: truststore.jks
              terminationMessagePolicy: File
              envFrom:
                -
                  configMapRef:
                    name: ${APP_NAME}
               # - 
                 # secretRef:
                    # name: link-secret-crapps-link-app-biometria
              #  - 
              #    secretRef:
               #     name: link-secret-link-app-biometria
               # - 
                #  secretRef:
                 #   name: link-secret-crapps-truststore
              env:
                - 
                  name: HTTP_PROXY
                  value: "http://10.132.64.23:3128"
                - 
                  name: HTTPS_PROXY
                  value: "http://10.132.64.23:3128"
                - name: passphrase
                  valueFrom:
                    secretKeyRef:
                      name: link-secret-truststore
                      key: passphrase
          dnsPolicy: ClusterFirst
          restartPolicy: Always
  - 
    apiVersion: autoscaling/v1
    kind: HorizontalPodAutoscaler
    metadata:
      name: ${APP_NAME} 
    spec:
      scaleTargetRef:
        kind: DeploymentConfig 
        name: ${APP_NAME} 
        apiVersion: apps.openshift.io/v1
        subresource: scale
      minReplicas: 1 
      maxReplicas: 2 
      targetCPUUtilizationPercentage: 80 
  -
    apiVersion: v1
    kind: ConfigMap
    metadata:
      name: ${APP_NAME}
    data:
      REDLINK_LDAP_HOST: ${REDLINK_LDAP_HOST}
  -
    apiVersion: v1
    kind: BuildConfig
    metadata:
      labels:
        app: ${APP_NAME}
      name: ${APP_NAME}
    spec:
      output:
        to:
          kind: ImageStreamTag
          name: "${APP_NAME}:latest"
      source:
        binary: {}
      strategy:
        dockerStrategy:
          dockerfilePath: Dockerfile
          env:
            -
              name: "MVN_USER"
              value: ${MVN_USER}
            - name: MVN_PWD
              valueFrom:
                secretKeyRef:
                  name: ${APP_NAME}
                  key: MVN_PWD
        type: Docker
  -
    apiVersion: v1
    kind: ImageStream
    metadata:
      labels:
        app: ${APP_NAME}
      name: ${APP_NAME}
  -
    kind: Secret
    apiVersion: v1
    metadata:
      name: ${APP_NAME}
    data:
      MVN_PWD: ${MVN_PWD}
    type: Opaque
  -
    kind: Gateway
    apiVersion: networking.istio.io/v1alpha3
    metadata:
      name: ${APP_NAME}
      labels:
        kiali_wizard: Gateway
    spec:
      servers:
        - hosts:
            - ${APP_NAME}.${GATEWAY_DOMAIN}
          port:
            name: https
            number: 443
            protocol: HTTPs
          tls:
            credentialName: app-cert
            mode: SIMPLE

      selector:
        istio: ingressgateway
  -
    kind: VirtualService
    apiVersion: networking.istio.io/v1alpha3
    metadata:
      name: ${APP_NAME}
      labels:
        app: ${APP_NAME}
    spec:
      hosts:
        - ${APP_NAME}.${GATEWAY_DOMAIN}
      gateways:
        - ${APP_NAME}
      http:
        - match:
            - uri:
                prefix: /
          route:
            - destination:
                host: ${APP_NAME}
                port:
                  number: 8080
  -
    kind: DestinationRule
    apiVersion: networking.istio.io/v1alpha3
    metadata:
      name: ${APP_NAME}
      labels:
        app: ${APP_NAME}
    spec:
      host: ${APP_NAME}
      trafficPolicy:
        tls:
          mode: ISTIO_MUTUAL

parameters:
  -
    displayName: "Application Name"
    name: APP_NAME
    required: true
    value: logservicesgateway
  -
    description: "MVN_PWD"
    name: MVN_PWD
    required: true
  -
    description: "MVN_USER"
    name: MVN_USER
    required: true
  -
    description: "REDLINK_LDAP_HOST"
    name: REDLINK_LDAP_HOST
    required: true
  -
    description: "GATEWAY_DOMAIN"
    name: GATEWAY_DOMAIN
    required: true